module VotedHelper
  def total_votes_color(value)
    value < 0 ? 'voting-counter_negative' : 'voting-counter_positive'
  end
end