module UsersHelper
  def avatar_for(user)

    if user.avatar.url
      image_tag user.avatar.url(:thumb)
    elsif user.oauth_avatar_url
      image_tag user.oauth_avatar_url
    else
      gravatar_id = Digest::MD5::hexdigest(user.email.to_s)
      gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?d=retro&amp;f=y"
      image_tag(gravatar_url, alt: user.email, class: "gravatar")
    end
  end
end
