class TagsController < ApplicationController
  def index; end
  def show
    @tag = Tag.find_by(name: params[:id])
    @questions = @tag.questions.includes(:user, :tags)
  end
end
