class BooksController < ApplicationController

  def index
    @book = Book.all
  end

  def show
    @book = Book.find(params[:id])
  end

  def new
    @book = Book.new
  end

  def create
    @book = Book.new(book_params)
    if @user.save
      flash[:succses] = 'Book created'
      redirect_to @book
    else
      render 'new'
    end
  end

  def edit
    @book = Book.find(params[:id])
  end

  def update
    @book = Book.find(params[:id])
    @book.update!(book_params)
    flash[:notice] = 'Книга обновлена'
    redirect_to @book
  end

  private

  def book_params
    params.require(:book).permit(:author, :description, :user_id)
  end
end
