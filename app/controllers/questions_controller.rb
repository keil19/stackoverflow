class QuestionsController < ApplicationController
  before_action :authenticate_user!, except: %i[index show]
  before_action :find_question, only: %i[show edit update destroy vote]
  impressionist actions: [:show], unique: [:session_hash]
  authorize_resource
  include Voted

  respond_to :html, except: %i[update vote unvote]
  respond_to :js, only: %i[update]
  respond_to :json, only: %i[vote unvote]
  def index
    @questions = Question.search(params[:search])
                         .includes(:tags, :user)
                         .paginate(page: params[:page], per_page: 5)
    @tags = Tag.first(10).map(&:name)
    # Tagging.group(:tag_id).count
  end

  def show
    @comments = @question.comments.includes(:user).sort_by(&:total_votes).reverse!
    @question.impressionist_count(filter: :session_hash)
  end

  def new
    respond_with @question = Question.new
  end

  def create
    @question = current_user.questions.new(question_params)
    flash[:notice] = 'Question successfully created' if @question.save
    respond_with @question
  end

  def edit; end

  def update
    if @question.update_attributes(question_params)
      redirect_to @question
    else
      render :edit
    end
  end

  def destroy
    respond_with @question.destroy
  end

  private

  def find_question
    @question = Question.find(params[:id])
  end

  def question_params
    params.require(:question).permit(:title, :body, :all_tags)
  end
end
