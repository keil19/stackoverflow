class RegistrationsController < Devise::RegistrationsController
  private

  def sign_up_params
    params.require(:user).permit(:login, :email, :country,
                                 :city, :address, :avatar, :role, :password,
                                 :password_confirmation)
  end

  def account_update_params
    params.require(:user).permit(:login, :email, :country,
                                 :city, :address, :avatar, :role, :password,
                                 :password_confirmation, :current_password)
  end

  protected

  def update_resource(resource, params)
    if params[:password].blank? && params[:password_confirmation].blank?
      params.delete(:password)
      params.delete(:password_confirmation)
      params.delete(:current_password)
    end
    resource.update_without_password(params)
  end
end
