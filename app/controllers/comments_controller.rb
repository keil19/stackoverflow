class CommentsController < ApplicationController
  before_action :authenticate_user!, except: %i[new index]
  before_action :find_question
  before_action :find_comment, only: %i[edit update destroy best]
  authorize_resource
  include Voted

  respond_to :js, except: %i[vote unvote best]
  respond_to :json, only: %i[vote unvote best]
  def new
    respond_with(@comment = Comment.new)
  end

  def create
    @comment = @question.comments.build(comment_params.merge(user_id: current_user.id))
    respond_to do |format|
      if @comment.save
        format.js
      else
        format.js { render status: 400 }
      end
    end
  end

  def edit; end

  def update
    respond_to do |format|
      format.js if @comment.update(comment_params)
    end
  end

  def destroy
    @comment.destroy
    respond_to do |format|
      format.js
    end
  end

  def best
    @question.comments.update_all best: false
    @comment.update_attribute :best, params[:best]
    respond_to do |format|
      format.json { render json: @comment }
    end
  end

  private

  def find_question
    @question = Question.find(params[:question_id])
  end

  def find_comment
    @comment = @question.comments.published.find(params[:id])
  end

  def comment_params
    params.require(:comment).permit(:content, :best)
  end
end
