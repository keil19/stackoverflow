class UsersController < ApplicationController
  authorize_resource
  before_action :find_user, only: %i[show edit update questions change_role]
  def index
    @users = User.all.paginate(page: params[:page], per_page: 15)
  end

  def questions
    @questions = @user.questions.includes(:tags)
  end

  def show; end

  def edit; end

  def update
    if @user.update(user_params)
       flash[:notice] = 'Your account has been updated'
       redirect_to @user
     else
       render :show
     end
  end

  def change_role
    if @user.update_attribute(:role, params[:role])
      flash[:notice] = 'Role changed'
      redirect_to @user
    else
      render :show
    end
  end

  private

  def find_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:login, :email, :country, :city,
                                 :address, :avatar, :role)
  end
end
