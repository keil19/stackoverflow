module Voted
  extend ActiveSupport::Concern
  included do
    before_action :find_votable, only: %i[vote unvote]
  end
  
  def vote
    @vote = @votable.votes.where(user: current_user, votable: @votable).first
    if @vote
      if @vote.mark.to_s == params[:vote]
        delete_vote
      else
        @vote.update_attribute :mark, params[:vote]
      end
    else
      @vote = @votable.votes.create(mark: params[:vote], user: current_user)
    end
    respond_to do |format|
      if @vote.save
        format.json { render json: @vote.attributes.merge(total_votes_count: @votable.total_votes) }
      else
        format.json { render json: @vote.errors.messages.values, status: :unprocessable_entity }
      end
    end
  end

  private

  def delete_vote
    Vote.where(user: current_user, votable: @votable).first.destroy
  end

  def find_votable
    @votable = model.find(params[:id])
  end

  def model
    controller_name.classify.constantize
  end
end