class ApplicationController < ActionController::Base
  include Pundit
  protect_from_forgery with: :exception
  def after_sign_in_path_for(resource)
    super resource
  end

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, alert: exception.message
  end

  protected

  def authenticate_user!(options = {})
    if user_signed_in?
      super(options)
    else
      redirect_to new_user_session_path,
                  alert: 'You need to sign in or sign up before continuing.'
    end
  end
end