$(document).on('turbolinks:load', function() {
    $('.ui.question').bind('ajax:success', function (e, data) {
       var $span = $(this).find('.voting > span');
       changeTextAndClass($span, data.total_votes_count);
    })
    $('.ui.comments').bind('ajax:success','.voting', function (e, data) {
        var $span = $('#comment_id_'+data.votable_id + ' .voting' ).find('span');
        changeTextAndClass($span, data.total_votes_count);
    })

    function voteColorClass(value){
       return value < 0 ? 'voting-counter_negative' : 'voting-counter_positive'
    }
    function changeTextAndClass(tag, total_votes_count){
        tag.text( total_votes_count ).removeClass();
        tag.addClass(voteColorClass(total_votes_count));
    }
});