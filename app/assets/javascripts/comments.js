$(document).on('turbolinks:load', function() {
     $('.ui.comments').on('click', '.edit-comment-link', function() {
        $this = $(this);
        var commentId = $this.data('comment-id');
        var form = $("#edit_comment_form_" + commentId);
        form.transition('scale');
        if ($this.hasClass('hide-form')){
            $this.text('Edit');
            $this.removeClass('hide-form');
        } else {
            $this.text('Hide edit form');
            $this.addClass('hide-form');
        }
        return false
    });

    $('.ui.comments').bind('ajax:success','.make-best', function(e, data) {
        $this = $(this);
        //detete disabled from button
        $iconCheck = $('.make-best i.green');
        $iconCheck.removeClass('green');
        $iconCheck.closest('button.ui.icon').removeAttr('disabled');
        //
        var $comment = $('#comment_id_' + data.id);
        $comment.find('.make-best button').attr("disabled", true);
        $comment.find('.make-best i').addClass('green');
    })
});