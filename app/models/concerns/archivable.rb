module Archivable
  extend ActiveSupport::Concern

  included do
    scope :published, -> { where(deleted: false) }
  end

  def delete
    self.deleted = true
    self.save
  end

  def revive
    self.update_attribute :deleted, false
  end

end