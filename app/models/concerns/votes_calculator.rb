module Votes_Calculator
  extend ActiveSupport::Concern
  def total_votes
    self.up_votes - self.down_votes
  end

  def up_votes
    self.votes.where(mark: true).size
  end

  def down_votes
    self.votes.where(mark: false).size
  end
end