class Question < ActiveRecord::Base
  include Votable
  include Votes_Calculator
  has_many :comments, dependent: :destroy
  has_many :taggings, dependent: :destroy
  has_many :tags, through: :taggings, dependent: :destroy
  belongs_to :user
  validates :title, :body, presence: true
  is_impressionable

  def self.search(search)
    if search.present?
      Question.where('title LIKE ? OR body LIKE ?',
                     "%#{search}%", "%#{search}%")
    else
      Question.all
    end
  end

  def all_tags
    self.tags.map(&:name).join(', ')
  end

  def all_tags=(names)
    names = names.split(',').map(&:strip).select { |t| t.size > 1 }
    self.tags = names.map do |name|
      Tag.where(name: name.strip).first_or_create!
    end
  end
end
