class Comment < ActiveRecord::Base
  include Archivable
  include Votable
  include Votes_Calculator
  belongs_to :question
  belongs_to :user
  validates :content, presence: true
  default_scope { where(deleted: false) }
end
