class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable, :recoverable,
         :confirmable, :rememberable, :trackable, :validatable,
         :omniauthable, omniauth_providers: %i[facebook]
  mount_uploader :avatar, AvatarUploader
  has_many :comments, dependent: :destroy
  has_many :questions, dependent: :destroy
  has_many :authorizations, dependent: :destroy
  has_many :votes
  enum role: { user: 0, admin: 1, moderator: 2 }

  validates :login, :country, :country, :city, :address, presence: true

  def change_role(user, role)
    user.update(role: role) if user
  end

  def self.from_omniauth(auth)
    authorization = Authorization.where(provider: auth.provider, uid: auth.uid.to_s).first
    return authorization.user if authorization
    email = auth[:info][:email]
    user = User.where(email: email).first
    if user
      user.create_auth(auth)
    else
      login = email.split('@').first
      password = Devise.friendly_token[0, 10]
      oauth_avatar_url = auth.info.image ? auth.info.image.gsub('http://', 'https://') : ''
      user = User.new(email: email, login: login, password: password,
                      password_confirmation: password,  oauth_avatar_url: oauth_avatar_url,
                      country: 'uk', city: 'kh', address: 'sh')
      user.skip_confirmation!
      user.save
      user.create_auth(auth)
    end
    user
  end

  def create_auth(auth)
    self.authorizations.create(provider: auth.provider, uid: auth.uid.to_s)
  end
end
