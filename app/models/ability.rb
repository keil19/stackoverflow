class Ability
  include CanCan::Ability

  def initialize(user)
    user ? auth_abilities(user) : guest_abilities
  end

  def auth_abilities(user)
    if user.admin?
      can :manage, :all
    elsif user.moderator?
      moderator_abilities(user)
    else
      user_abilities(user)
    end
  end

  def guest_abilities
    can :read, :all
    can :questions, User
    cannot :index, User
  end

  def user_abilities(user)
    guest_abilities
    can :vote, Question do |votable|
      votable.user != user
    end
    can :vote, Comment do |votable|
      votable.user != user
    end
    can :best, Comment do |comment|
      comment.question.user == user
    end
    can %i[create edit update destroy],
        [Question, Comment, User], user: user
  end

  def moderator_abilities(user)
    user_abilities(user)
    can %i[edit update destroy], [Question, Comment]
  end
end
