Rails.application.routes.draw do
  use_doorkeeper
  devise_for :users, controllers: { registrations: 'registrations',
                                    omniauth_callbacks: 'users/omniauth_callbacks' }
  root 'questions#index'

  resources :users do
    member do
      get :questions
      patch :change_role
    end
  end

  concern :votable do
    member do
      post :vote
    end
  end

  resources :questions, concerns: [:votable] do
    resources :comments, concerns: [:votable] do
      member do
        post :best
      end
    end
  end

  resources :tags, only: %i[show index]

  resource :search, only: %i[new show edit]

  get '/auth/:provider/callback', to: 'registrations#create'

  mount ActionCable.server => '/cable'

  namespace :api do
    namespace :v1 do
      resource :profiles do
        get :me, on: :collection
      end
      resources :questions
    end
  end
end
