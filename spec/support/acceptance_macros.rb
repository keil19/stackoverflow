module AcceptanceMacros
  def sign_in(user)
    visit new_user_session_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: user.password
    click_on 'Log in'
  end

  def make_user
    user = User.new(login: 'user', email: 'user@gmail.com', country: 'ukr', city: 'kh', address: 'sym', password: 'qweqwe')
    user.skip_confirmation!
    user.save!
    user
  end
end