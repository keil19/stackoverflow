FactoryGirl.define do
  factory :question do
    title "MyQuestion"
    body "MyQuestion"
    user
  end
  factory :invalid_question, class: "Question" do
    title ""
    body ""
    user
  end
end
