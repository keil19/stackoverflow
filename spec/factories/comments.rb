FactoryGirl.define do
  factory :comment do
    content "MyComment"
    question
    user
  end
  factory :invalid_comment, class: 'Comment' do
    content nil
    question nil
    user nil
  end
end
