FactoryGirl.define do
  sequence :email do |n|
    "user#{n}@gmail.com"
  end
  sequence :login do |n|
    "user#{n}"
  end
  factory :user do
    email
    login
    country 'ukr'
    city 'kh'
    address 'sym'
    password 'qweqwe'
    password_confirmation 'qweqwe'
    questions []
    authorizations []
    comments []
    confirmed_at          Time.now
  end
end