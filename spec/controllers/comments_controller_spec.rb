require 'rails_helper'

RSpec.describe CommentsController, type: :controller do
  let!(:question) { create(:question) }
  sign_in_user
  describe 'POST #create' do
    context 'with valid attributes' do
      let(:request) {  post :create, comment: attributes_for(:comment), question_id: question, format: 'js' }
      it 'save the new question in db' do
        expect { request }.to change(Comment, :count).by(1)
      end
      it 'render create tamplate' do
        request
        expect(response).to render_template :create
      end
    end
    context 'with invalid attributes' do
      let(:invalid_request) { post :create, comment: attributes_for(:invalid_comment), question_id: question, format: 'js' }
      it 'does not save comment in db' do
        expect { invalid_request }.to_not change(Comment, :count)
      end
      # it 're-render new view' do
      #   invalid_request
      #   expect(response).to redirect_to questions_path(question)
      # end
    end
  end
  describe 'PATCH #update' do
    let(:comment) { create(:comment, question: question) }
    it 'with valid attributes' do
      patch :update, id: comment, question_id: question, comment: attributes_for(:comment)
      comment.reload
      expect(assigns(:comment)).to eq comment
    end
  end
end
