require_relative 'acceptance_helper'

feature 'Comment edit', %q{
  In order to fix mistake
  As an author of answer
  I'd like at be able to edit my answer
}  do
  given(:user) { make_user }
  given!(:question) { create(:question) }
  given!(:comment) { create(:comment, question: question, user: user) }
  describe 'Auth user' do
    before do
      sign_in user
      visit question_path(question)
    end
    scenario 'sees link to edit' do
      within '.comments' do
        expect(page).to have_link('Edit', class: 'edit-comment-link')
      end
    end
    scenario 'try to edit his comment' do
      first('.edit-comment').click
      fill_in 'comment[content]', with: 'edited comment'
      click_on 'Save'
      expect(page).to_not have_content comment.content
      expect(page).to have_content 'edited comment'
      expect(current_path).to eq question_path(question)
    end
    scenario 'try to edit other user\'s comment' do

    end
  end
  describe 'Non-auth user' do
    scenario 'Unauthenticated user try to edit comment' do
      visit question_path(question)
      expect(page).to_not have_link 'edit'
    end
  end
end