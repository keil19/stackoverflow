require_relative 'acceptance_helper'

feature 'User sing_in', %q{
  In oreder to be able to ask question
  As an user
  I want ot be able to sjign in
} do
  given(:user) { make_user }
  scenario 'Registared user try to sign in' do
    sign_in(user)
    expect(page).to have_content 'Signed in successfully'
    expect(current_path).to eq root_path
  end
  scenario 'Non-registered user try to sign in' do
    visit new_user_session_path
    fill_in 'Email', with: 'user2@gmail.com'
    fill_in 'Password', with: 'qweqwe1'
    click_on 'Log in'
    expect(page).to have_content 'Invalid Email or password'
    expect(current_path).to eq new_user_session_path
  end
end