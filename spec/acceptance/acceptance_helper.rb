require 'rails_helper'
require 'capybara-webkit'
RSpec.configure do |config|
  Capybara.javascript_driver = :webkit
  config.use_transactional_fixtures = false
  config.before(:suite)              { DatabaseCleaner.clean_with(:truncation) }
  config.before(:each)               { DatabaseCleaner.strategy = :transaction }
  config.before(:each, js: true)     { DatabaseCleaner.strategy = :truncation }
  config.before(:each, sphinx: true) { DatabaseCleaner.strategy = :deletion }
  config.before(:each)               { DatabaseCleaner.start }
  config.after(:each)                { DatabaseCleaner.clean }
end