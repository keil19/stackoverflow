require_relative 'acceptance_helper'

feature 'User comment', %q{
  In order
  As a
  I want to
} do
  given(:user) { make_user }
  given(:question) { create(:question) }
  scenario 'Authenticate user create comment', js: true do
    sign_in(user)
    visit question_path(question)
    fill_in 'comment[content]', with: 'My awesome comment'
    click_on 'Add comment'
    expect(current_path).to eq question_path(question)
    within '.comments' do
      expect(page).to have_content 'My awesome comment'
    end
  end
  scenario 'Authenticated user create comment with invalid data' do
    sign_in(user)
    visit question_path(question)
    click_on 'Add comment'
    expect(page).to have_content("can't be blank")
    expect(current_path).to eq question_path(question)
  end
  scenario 'Non-authenticate user create comment', js: true do
    visit question_path(question)
    expect(page).to have_content 'You need to sign in or sign up'
  end
end