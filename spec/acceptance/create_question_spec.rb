require_relative 'acceptance_helper'

feature 'Create question', %q{
  get answers
  as authenticated user
} do
  given(:user) { make_user }
  scenario 'Auth user create question' do
    sign_in(user)
    visit root_path
    click_on 'Ask Question'
    fill_in 'question[title]', with: 'Rails?'
    fill_in 'question[body]', with: 'Rails'
    click_on 'Save'
    expect(page).to have_content 'Rails?'
  end
  scenario 'Non auth user try create question' do
    visit root_path
    click_on 'Ask Question'
    expect(page).to have_content 'You need to sign in or sign up'
  end
end