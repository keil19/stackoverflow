require 'rails_helper'

RSpec.describe User do
  it { should validate_presence_of :email }
  it { should validate_presence_of :login }
  it { should validate_presence_of :password }
  it { should validate_presence_of :country }
  it { should validate_presence_of :city }
  it { should validate_presence_of :address }

  describe  'find_for_ouath' do
    let!(:user) { create(:user) }
    let(:auth) { OmniAuth::AuthHash.new(provider: 'facebook', uid: '123123') }
    let(:user_after_auth) { User.from_omniauth(auth) }
    context 'user already has auth' do
      it 'return user' do
        user.authorizations.create(provider: 'facebook', uid: '123123')
        expect(User.from_omniauth(auth)).to eq user
      end
    end
    context 'user has not auth' do
      context 'user already exists' do
        let(:auth) do
          OmniAuth::AuthHash.new(provider: 'facebook', uid: '123123',
                                 info: { email: user.email })
        end
        it 'does not create new user' do
          expect { user_after_auth }.to_not change { User.count }
        end
        it 'create auth for user' do
          expect { user_after_auth }.to change { user.authorizations.count }.by(1)
        end
        it 'creates auth with provider and uid' do
          authorization = user_after_auth.authorizations.first
          expect(authorization.provider).to eq auth.provider
          expect(authorization.uid).to eq auth.uid
        end
      end
      context 'user does not exists' do
        let(:auth) do
          OmniAuth::AuthHash.new(provider: 'facebook',
                                 uid: '123123', info: { email: 'new@user.com' })
        end
        it 'creates new user' do
          expect { user_after_auth }.to change { User.count }.by(1)
        end
        it 'fills user email' do
          expect(user_after_auth.email).to eq auth.info.email
        end
        it 'creates auth for user' do
          expect(user_after_auth.authorizations).to_not be_empty
        end
        it 'creates auth with provider and uid' do
          user_auth = user_after_auth.authorizations.first
          expect(user_auth.provider).to eq auth.provider
          expect(user_auth.uid).to eq auth.uid
        end
      end
    end
  end
end