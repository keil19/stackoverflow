require 'rails_helper'

RSpec.describe Ability, type: :model do
  subject(:ability) { Ability.new(user) }

  describe 'for guest' do
    let(:user) { nil }
    it { should be_able_to :read, Question }
    it { should be_able_to :show, User }
    it { should be_able_to :read, Comment }
    it { should_not be_able_to :index, User }
  end

  describe 'for admin' do
    let(:user) { create :user, role: 1 }
    it { should be_able_to :manage, :all }
  end

  describe 'for user' do
    let(:user) { create :user, role: 0 }
    let(:user2) { create :user, role: 0 }
    it { should be_able_to :read, Question }
    it { should be_able_to :read, Comment }
    it { should be_able_to :show, User }
    context 'User' do
      it { should_not be_able_to :index, User }
    end
    context 'Question' do
      it { should be_able_to :create, Question }

      it { should be_able_to :update, create(:question, user: user), user: user }
      it { should_not be_able_to :update, create(:question, user: user2), user: user }

      it { should_not be_able_to :vote, create(:question, user: user), user: user }
      it { should be_able_to :vote, create(:question, user: user2), user: user }
    end
    context 'Comment' do
      it { should be_able_to :create, Comment }
      it { should be_able_to :update, create(:comment, user: user), user: user }
      it { should_not be_able_to :update, create(:comment, user: user2), user: user }

      it { should_not be_able_to :vote, create(:comment, user: user), user: user }
      it { should be_able_to :vote, create(:comment, user: user2), user: user }
    end
  end

  describe 'for moderator' do
    let(:user) { create :user, role: 2 }
    let(:user2) { create :user, role: 0 }
    it { should be_able_to :read, Question }
    it { should be_able_to :read, Comment }
    context 'User' do
      it { should_not be_able_to :index, User }
    end
    context 'Question' do
      it { should be_able_to :create, Question }
      it { should be_able_to :update, create(:question, user: user), user: user }
      it { should be_able_to :update, create(:question, user: user2), user: user }
    end
    context 'Comment' do
      it { should be_able_to :create, Comment }
      it { should be_able_to :update, create(:comment, user: user), user: user }
      it { should be_able_to :update, create(:comment, user: user2), user: user }
    end
  end
end
