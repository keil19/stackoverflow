require 'rails_helper'

RSpec.describe Question, type: :model do
  it { should have_many(:tags).dependent(:destroy) }
  it { should have_many(:taggings).dependent(:destroy) }
  it { should have_many(:comments).dependent(:destroy) }
  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:body)  }
end
