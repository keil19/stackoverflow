require 'rails_helper'

describe 'Question API' do
  describe 'GET /index' do
    context 'unauthorized' do
      it 'returns 401 status if no access_token' do
        get '/api/v1/questions', params: { format: :json }
        expect(response.status).to eq 401
      end

      it 'returns 401 status if access_token in invalid' do
        get '/api/v1/questions', params: { format: :json, access_token: '1235' }
        expect(response.status).to eq 401
      end
    end
    context 'authorized' do
      let!(:me) { create :user }
      let(:access_token) { create(:access_token, resource_owner_id: me.id) }
      let!(:questions) { create_list(:question, 2) }
      let(:question) { questions.first }
      let!(:comment) { create(:comment, question: question) }

      before { get '/api/v1/questions', params: { format: :json, access_token: access_token.token } }      

      it 'return 200' do 
        expect(response).to be_success
      end  

      it 'return array of questions' do
        expect(response.body).to have_json_size(2)
      end

      %w(id body title).each do |attr|
        it "question obj contains #{attr}"  do
          expect(response.body).to be_json_eql(question.send(attr.to_sym).to_json).at_path("0/#{attr}")
        end
      end


      
    end
  end
end
