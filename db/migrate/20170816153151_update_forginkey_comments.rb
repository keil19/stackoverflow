class UpdateForginkeyComments < ActiveRecord::Migration[5.0]
  def change
    remove_foreign_key :comments, :questions
    add_foreign_key :comments, :questions, on_delete: :cascade
  end
end
