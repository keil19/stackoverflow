class RemoveUserIdFromBookAndAddReference < ActiveRecord::Migration
  def change
    remove_column :books, :user_id
    add_reference :books,:user, index: true
  end
end
