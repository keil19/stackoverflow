class AddOuthAvatarUrlToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :oauth_avatar_url, :string
  end
end
