class UpdateForeignKeyTagForTaggings < ActiveRecord::Migration[5.0]
  def change
    remove_foreign_key :taggings, :tags
    add_foreign_key :taggings, :tags, on_delete: :cascade
  end
end
