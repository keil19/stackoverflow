class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.string :author
      t.string :description
      t.date   :publication_date
      t.timestamps null: false
    end
  end
end
