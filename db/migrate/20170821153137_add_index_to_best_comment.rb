class AddIndexToBestComment < ActiveRecord::Migration[5.0]
  def change
    add_index :comments, :best
  end
end
