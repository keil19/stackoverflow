class ChangeVoteType < ActiveRecord::Migration[5.0]
  def change
    change_column :votes, :mark, 'boolean USING CAST(mark AS boolean)'
  end
end
