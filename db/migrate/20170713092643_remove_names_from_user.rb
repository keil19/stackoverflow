class RemoveNamesFromUser < ActiveRecord::Migration
  def change
    remove_column :users, :first_name, :string
    remove_column :users, :last_name, :string
    remove_column :users, :phone, :string
    remove_column :users, :uuid, :string
  end
end
