class AddIndexMarkToVotes < ActiveRecord::Migration[5.0]
  def change
    add_index :votes, :mark
  end
end
