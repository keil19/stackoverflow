class RemoveViewsColumnFromQuestion < ActiveRecord::Migration
  def change
    remove_column :questions, :views
  end
end
