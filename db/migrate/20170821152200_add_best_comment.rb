class AddBestComment < ActiveRecord::Migration[5.0]
  def change
    add_column :comments, :best, :boolean, default: false, index: true
  end
end
