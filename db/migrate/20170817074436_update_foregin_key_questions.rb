class UpdateForeginKeyQuestions < ActiveRecord::Migration[5.0]
  def change
    remove_foreign_key :taggings, :questions
    add_foreign_key :taggings, :questions, on_delete: :cascade
  end
end
